
# Cloning the source code into your local machine

You can use one of the following approaches to clone the source code: 

1. Type in the terminal if you have git installed and configured 
```  
    git clone https://malhamdoosh@bitbucket.org/malhamdoosh/secret-revealed-workshop.git 
    cd secret-revealed-workshop 

``` 
2. Download https://www.sourcetreeapp.com, install it and follow the instructions here https://www.sourcetreeapp.com/?utm_source=internal&utm_medium=link&utm_campaign=clone_repo_mac#step-2 
3. Visit https://bitbucket.org/malhamdoosh/secret-revealed-workshop/downloads/?tab=branches and then select "zip" from the Download column

# Preparing your working environment 

## Windows

1.    Install the package manager Miniconda (or Anaconda). Miniconda is a stripped-down version of Anaconda which will suffice. Choose the Python 3.7 version for your platform from here https://docs.conda.io/en/latest/miniconda.html for your platform.
2.    Click next until the installation process completes.
3.    Follow this step if you are using a CSL issued laptop. In C:\Users\<your id>\, create a text file named .condarc (without the .txt extension) and paste the following into the file:
``` 
    proxy_servers:
       http: 165.225.98.36:80
       https: 165.225.98.36:80 
``` 
4.    Search and open “Anaconda Prompt” from the search bar.
5.    Type:
```
    conda create -y -n basicnn
    
    conda activate basicnn
    
    conda install -y -c anaconda tensorflow==1.13.1
    
    conda install -y jupyter matplotlib
    
    jupyter notebook
```
6.    A browser window should open. Navigate to where cloned the workshop repository and click on basic_cnn.ipynb.


## MacOS
 
1.    Install the package manager Miniconda (or Anaconda). Miniconda is a stripped-down version of Anaconda which will suffice. Choose the Python 3.7 version for your platform from here https://docs.conda.io/en/latest/miniconda.html for your platform.
2.    Open a terminal window (cmd space for spotlight search and type “Terminal”).
3.    Type: 
```
    cd ~/Downloads (or substitute with the location where the file was downloaded)
    bash Miniconda3-latest-MacOSX-x86_64.sh -b (or with Anaconda’s file if that was downloaded instead)
```
4.    Follow this step if you are using a CSL issued laptop. In your home directory (/Users/<your id>/), create a text file named .condarc (without the .txt extension) and paste the following into the file:
```
    proxy_servers:
        http: 165.225.98.36:80
        https: 165.225.98.36:80
```
5.    Completely close (cmd q) and open a new terminal window (see 2.).
6.    Type:
```
    conda create -y -n basicnn
    
    conda activate basicnn
    
    conda install -y -c anaconda tensorflow==1.13.1
    
    conda install -y jupyter matplotlib
    
    jupyter notebook
```
7.    A browser window should open. Navigate to where cloned the workshop repository click on basic_cnn.ipynb.


## Ready-to-use conda environment creation 

If you followed the steps above and the code in the notebook was not executed successfully, you can try to install the exported conda environment as follows:

1. Navigate to the workshop repository folder

2. Type in the terminal (or Anaconda Prompt in Windows)
```
conda env create -f environment.yml

conda activate ml

jupyter notebook 
```